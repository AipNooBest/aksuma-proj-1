use std::env;

fn main(){
    let app_name = "devops";
    let args: Vec<String> = env::args().collect();

    if args.len() != 2 {
        println!("Использование: ./{app_name} <символ>");
        std::process::exit(1);
    }

    let input = &args[1];
    if input.len() != 1 {
        println!("Использование: ./{app_name} <символ>");
        std::process::exit(1);
    };
    let character: char = input.chars().next().unwrap_or(' ');

    let input: u32 = match character.to_digit(10) {
        Some(i) => i,
        None => character as u32
    };

    println!("{input}");
}